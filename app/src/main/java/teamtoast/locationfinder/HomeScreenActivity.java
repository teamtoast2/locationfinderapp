package teamtoast.locationfinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

//Framework imports
import com.team_toast.framework.AppManagement.Permissions;
import com.team_toast.framework.StateManager.Caretaker;
import com.team_toast.framework.AppManagement.Popup;
import com.team_toast.framework.StateManager.StringOriginator;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * The initial home screen of the application
 */
public class HomeScreenActivity extends AppCompatActivity {

    public final static String SEARCH_STRING = "";

    //Memento design pattern variables
    public Caretaker caretaker;
    public StringOriginator originator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        //Initial permissions requests
        Permissions permissions = new Permissions();
        permissions.requestFinePermissions(this);
        permissions.requestCoarsePermissions(this);

        //Load all asset files
        try {
            Assets.loadLocationFiles(this.getApplicationContext());
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Initialise memento variables
        caretaker = new Caretaker();
        originator = new StringOriginator();

    }

    /**
     * Executes when activity resumes
     */
    @Override
    public void onResume() {
        super.onResume();

        //Clear search box text
        EditText search_edTxt = (EditText) findViewById(R.id.edit_message);
        search_edTxt.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     *  Processes the search function
     * @param view
     */
    public void sendSearchQuery(View view) {

        //Get string from editText box
        EditText search_edTxt = (EditText) findViewById(R.id.edit_message);
        String message = search_edTxt.getText().toString();

        //Check if text has been entered
        if(message.isEmpty()) {
            //No text entered
            Popup p = new Popup();
            p.newPopup(this, "Error.", "Please make sure you have filled in the search box.");
            return;
        }

        //Text has been entered, continue..

        //Save text to memento state
        originator.setMyValue(message);
        caretaker.addMemento( originator.saveToMemento() );

        //Put message into intent and start activity.
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra(SEARCH_STRING, message);
        startActivity(intent);
    }

    /**
     *  Executes when the undo button is clicked.
     *
     * @param view  The view of the activity
     */
    public void undoState(View view) {

        //Get previous state
        originator.restoreFromMemento(caretaker.getMemento());
        String message = originator.getMyValue();

        //Apply text to search bar
        EditText search_edTxt = (EditText) findViewById(R.id.edit_message);
        search_edTxt.setText(message);
    }

    /** Called when the user clicks the department button */
    public void selectDepartment(View view) {
        Intent intent = new Intent(this, CategoryListActivity.class);
        intent.putExtra("listType", CampusPlace.ListType.DEPARTMENT);
        startActivity(intent);
    }

    /** Called when the user clicks the food/drink button */
    public void selectFoodDrink(View view) {
        Intent intent = new Intent(this, CategoryListActivity.class);
        intent.putExtra("listType", CampusPlace.ListType.FOODDRINK);
        startActivity(intent);
    }

    /** Called when the user clicks the general location button */
    public void selectGeneralLocation(View view) {
        Intent intent = new Intent(this, CategoryListActivity.class);
        intent.putExtra("listType", CampusPlace.ListType.GENERAL);
        startActivity(intent);
    }

}
