package teamtoast.locationfinder;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Activity that displays a list of the selected catergory (DEPARTMENT, GENERAL, FOODDRINK, or
 * a searched word)
 */
public class CategoryListActivity extends ListActivity {

    private ArrayList<CampusPlace> placeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        ListView listView1 = (ListView) findViewById(android.R.id.list);

        //Retrieve the intent
        Intent intent = getIntent();
        //Get the enum list type sent as an extra
        CampusPlace.ListType listType = (CampusPlace.ListType) intent.getSerializableExtra("listType");

        //Set the page title depending on which category is being viewed and load the relevant
        //category into the activity's placeList
        TextView title = (TextView) findViewById(R.id.textView);
        if(listType.equals(CampusPlace.ListType.DEPARTMENT)){
            placeList = Assets.getDepartmentList();
            title.setText("Department");
        }else if(listType.equals(CampusPlace.ListType.FOODDRINK)){
            placeList = Assets.getFoodDrinkList();
            title.setText("Food/Drink");
        }else if(listType.equals(CampusPlace.ListType.GENERAL)){
            placeList = Assets.getGeneralLocationList();
            title.setText("General Locations");
        }

        //Dynamically create a list on the page to display all elements of this activity's placeList
        ArrayAdapter<CampusPlace> adapter = new ArrayAdapter<CampusPlace>(getListView().getContext(), R.layout.listtext, placeList);
        getListView().setAdapter(adapter);
        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String item = ((TextView)view).getText().toString();
                selectCampusPlace(view, position);
            }
        });
    }

    /**
     *  Called when the user selects a location from the list.
     *  Builds an intent including the elements list position and its category type.
     */
    public void selectCampusPlace(View view, int position) {
        Intent intent = new Intent(this, SelectedCampusPlaceActivity.class);
        String message = ""+position;
        intent.putExtra("ID",message);
        intent.putExtra("TYPE", placeList.get(0).getType());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_department, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
