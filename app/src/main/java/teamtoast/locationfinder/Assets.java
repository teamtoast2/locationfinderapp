package teamtoast.locationfinder;

import android.content.Context;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Class to handle any application wide
 */
public class Assets {

    public static ArrayList<CampusPlace> foodDrinkList;
    public static ArrayList<CampusPlace> departmentList;
    public static ArrayList<CampusPlace> generalLocationsList;

    public static ArrayList<CampusPlace> getDepartmentList(){
        return departmentList;
    }

    public static ArrayList<CampusPlace> getFoodDrinkList(){
        return foodDrinkList;
    }

    public static ArrayList<CampusPlace> getGeneralLocationList(){
        return generalLocationsList;
    }

    //Load in all the list XML files
    public static void loadLocationFiles(Context appContext) throws IOException, XmlPullParserException {
        ApplicationXmlParser parser = new ApplicationXmlParser(appContext);
        parser.loadFile("campusplacesdepartments.xml");
        parser.loadFile("campusgenerallocations.xml");
        parser.loadFile("campusplaceseatdrink.xml");
    }

}
