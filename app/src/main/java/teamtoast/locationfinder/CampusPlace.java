package teamtoast.locationfinder;

import java.io.Serializable;

/**
 * Created by agf13qmu on 16/11/2015.
 */
public class CampusPlace {

    private String name;
    private String description;
    private int id;
    private ListType type;
    private com.team_toast.framework.Location.Location location;

    //enum that refers to each different type of list
    public enum ListType {
        DEPARTMENT, FOODDRINK, GENERAL
    }

   //constructor which instanties the Location object
    public CampusPlace(){
        location = new com.team_toast.framework.Location.AndroidGpsLocation();
    }

    public void setType(ListType type){
        this.type = type;
    }

    public ListType getType(){
        return this.type;
    }

    public void setName(String newName){
        this.name = newName;
    }

    public void setDescription(String newDescription){
        this.description = newDescription;
    }

    public String getName(){
        return this.name;
    }

    public String getDescription(){
        return this.description;
    }

    public void setId(int newId){ this.id = newId; }

    public int getId(){ return this.id; }

    public com.team_toast.framework.Location.Location getLocation(){
        return location;
    }

    @Override
    public String toString() {
        return name;
    }
}
