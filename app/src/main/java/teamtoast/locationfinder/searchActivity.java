package teamtoast.locationfinder;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity that handles searching for a location and displaying any results
 */
public class SearchActivity extends ListActivity {

    private List<CampusPlace> listClone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //Get the search string from the intent
        Intent intent = getIntent();
        String message = intent.getStringExtra(HomeScreenActivity.SEARCH_STRING);

        //Search through each available category arraylist in Assets and check if there is a match
        //to the search string. If a match is found, add it to the listclone arraylist.
        listClone = new ArrayList<CampusPlace>();
        for (CampusPlace place : Assets.getDepartmentList()) {
            if(place.getName().matches("(?i).*"+message+".*")){
                listClone.add(place);
            }
        }
        for (CampusPlace place : Assets.getFoodDrinkList()) {
            if(place.getName().matches("(?i).*"+message+".*")){
                listClone.add(place);
            }
        }
        for (CampusPlace place : Assets.getGeneralLocationList()) {
            if(place.getName().matches("(?i).*"+message+".*")){
                listClone.add(place);
            }
        }

        //Create a listview to display any matching search results
        ListView listView1 = (ListView) findViewById(android.R.id.list);
        ArrayAdapter<CampusPlace> adapter = new ArrayAdapter<CampusPlace>(getListView().getContext(), R.layout.listtext, listClone);
        getListView().setAdapter(adapter);
        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String item = ((TextView) view).getText().toString();
                locationSelected(view, position);
            }
        });
    }

    /**
     * Called when user selects a location from the list of search results. Creates an intent to
     * forward to the next activity.
     * @param view
     * @param position
     */
    public void locationSelected(View view, int position) {
        //Create an intent containing the locations listType and element position in it's relevant
        //Assets arraylist
        Intent intent = new Intent(this, SelectedCampusPlaceActivity.class);
        intent.putExtra("ID", listClone.get(position).getId()+"");
        if(listClone.get(position).getType().equals(CampusPlace.ListType.DEPARTMENT)) {
            intent.putExtra("TYPE", CampusPlace.ListType.DEPARTMENT);
        }else if(listClone.get(position).getType().equals(CampusPlace.ListType.FOODDRINK)){
            intent.putExtra("TYPE", CampusPlace.ListType.FOODDRINK);
        }else if(listClone.get(position).getType().equals(CampusPlace.ListType.GENERAL)){
            intent.putExtra("TYPE", CampusPlace.ListType.GENERAL);
        }
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
