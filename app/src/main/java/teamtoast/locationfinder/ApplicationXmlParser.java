package teamtoast.locationfinder;

import android.content.Context;

import com.team_toast.framework.DataInterchange.AndroidXmlParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Application specific subclass to handle specific XML parsing
 */
public class ApplicationXmlParser extends AndroidXmlParser{

    public ApplicationXmlParser(Context appContext){
        super(appContext);
    }

    @Override
    public void parseData() throws XmlPullParserException, IOException{
        ArrayList<CampusPlace> campusPlaces = null;
        int eventType = parser.getEventType();
        CampusPlace newCampusPlace = null;
        CampusPlace.ListType type = null;

        //Whilst the end of the document has not been reached
        while (eventType != XmlPullParser.END_DOCUMENT){
            String name = null;
            //switch dependant on the parsers event type
            switch (eventType){
                //Create a new arraylist if at the beginning of a document
                case XmlPullParser.START_DOCUMENT:
                    campusPlaces = new ArrayList();
                    break;
                //Set various object variables dependant on what is being read in the XML file
                case XmlPullParser.START_TAG:
                    name = parser.getName();
                    //Set the object type, DEPARTMENT, GENERAL or FOODDRINK
                    if(name.equals("departmentList")){
                        type = CampusPlace.ListType.DEPARTMENT;
                    }else if(name.equals("generallocations")){
                        type = CampusPlace.ListType.GENERAL;
                    }else if(name.equals("eatDrinkList")){
                        type = CampusPlace.ListType.FOODDRINK;
                    }
                    //Begin creating a new campusPlace object when encountering the beginning of
                    //one in the XML file
                    if (name.equals("campusPlace")){
                        newCampusPlace = new CampusPlace();
                        newCampusPlace.setType(type);
                    }//if the campusPlace object is not null, populate variables as they are parsed.
                    else if (newCampusPlace != null){
                        if (name.equals("id")){
                            newCampusPlace.setId(Integer.parseInt(parser.nextText()));
                        }
                        if (name.equals("name")){
                            newCampusPlace.setName(parser.nextText());
                        } else if (name.equals("description")){
                            newCampusPlace.setDescription(parser.nextText());
                        } else if (name.equals("latitude")){
                            newCampusPlace.getLocation().setLatitude(Float.parseFloat(parser.nextText()));
                        } else if (name.equals("longitude")){
                            newCampusPlace.getLocation().setLongitude(Float.parseFloat(parser.nextText()));
                        }
                    }
                    break;
                //When the end tag is encountered add the campus place to the arraylist currently
                //being populated
                case XmlPullParser.END_TAG:
                    name = parser.getName();
                    if (newCampusPlace != null){
                        campusPlaces.add(newCampusPlace);
                        newCampusPlace = null;
                    }
            }
            eventType = parser.next();
        }
        //Save the completed arraylist to the specific list in Assets when complete
        if(campusPlaces.get(0).getType().equals(CampusPlace.ListType.DEPARTMENT)) {
            Assets.departmentList = campusPlaces;
        }else if(campusPlaces.get(0).getType().equals(CampusPlace.ListType.GENERAL)) {
            Assets.generalLocationsList = campusPlaces;
        }else if(campusPlaces.get(0).getType().equals(CampusPlace.ListType.FOODDRINK)) {
            Assets.foodDrinkList = campusPlaces;
        }
    }

}
