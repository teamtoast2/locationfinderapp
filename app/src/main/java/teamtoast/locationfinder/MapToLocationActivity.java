package teamtoast.locationfinder;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

//Framework
import com.team_toast.framework.Maps.AndroidMap;

/**
 * Activity to handle giving a user directions to a location on a map
 */
public class MapToLocationActivity extends FragmentActivity{

    private com.team_toast.framework.Location.Location appLocation;
    private AndroidMap gMap;
    private CampusPlace campusPlace;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_to_location);

        //Retrieve the category type, and the campus locations element position within
        // it's category arraylist
        Intent intent = getIntent();
        CampusPlace.ListType listType = (CampusPlace.ListType) intent.getSerializableExtra("TYPE");
        String ID = intent.getStringExtra("ID");
        int listID = Integer.parseInt(ID);

        //Retrieve the campusPlace object that is the current destination
        if(listType == CampusPlace.ListType.DEPARTMENT){
            campusPlace = Assets.getDepartmentList().get(listID);
        }else if(listType == CampusPlace.ListType.FOODDRINK){
            campusPlace = Assets.getFoodDrinkList().get(listID);
        }else if(listType == CampusPlace.ListType.GENERAL){
            campusPlace = Assets.getGeneralLocationList().get(listID);
        }

        //Display the destinations name on the page
        TextView placeName = (TextView) findViewById(R.id.placeName);
        placeName.setText(campusPlace.getName());


        //Create a SupportMapFragment using the map ID in the XML layout
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        //Create a new AndroidMap object using the SupportMapFragment, and passing in the actual map
        //making use of AndroidMap's singleton design pattern
        gMap = AndroidMap.getInstance();
        gMap.initialise(mapFragment, ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap());
        //Set the maps default position over UEA
        gMap.setMapDefaultPosition(new LatLng(52.621895, 1.239026));

        //Initialise a location object for current location
        appLocation = new com.team_toast.framework.Location.AndroidGpsLocation(this);
        appLocation.initialiseLocationManager(this);
        //Call the method to update the displayed route on the on screen map. The number at the end
        //is the refresh rate for rechecking location within the methods thread.
        gMap.updateRoute(appLocation, campusPlace.getLocation(), 20000);

    }

    /** Called when the user clicks the Home button */
    public void goToHomePage(View view) {
        Intent intent = new Intent(this, HomeScreenActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map_to_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
