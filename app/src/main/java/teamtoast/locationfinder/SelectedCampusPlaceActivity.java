package teamtoast.locationfinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.team_toast.framework.AppManagement.Permissions;

/**
 * Activity that displays a selected campus location
 */
public class SelectedCampusPlaceActivity extends AppCompatActivity {

    private int listID;
    private CampusPlace.ListType listType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_campus_place);

        //Retrieve details from the intent
        Intent intent = getIntent();
        String ID = intent.getStringExtra("ID");
        listID = Integer.parseInt(ID);
        listType = null;

        //If a ListType enum has been sent, retrieve it
        Bundle extras = intent.getExtras();
        if(extras.containsKey("TYPE")) {
            listType = (CampusPlace.ListType) intent.getSerializableExtra("TYPE");
        }

        //Dynamically set the location name and description depending on the location selected
        if(listType.equals(CampusPlace.ListType.DEPARTMENT)) {
            TextView placeName = (TextView) findViewById(R.id.placeName);
            placeName.setText(Assets.getDepartmentList().get(listID).getName());
            TextView description = (TextView) findViewById(R.id.description);
            description.setText(Assets.getDepartmentList().get(listID).getDescription());
        }else if(listType.equals(CampusPlace.ListType.FOODDRINK)){
            TextView placeName = (TextView) findViewById(R.id.placeName);
            placeName.setText(Assets.getFoodDrinkList().get(listID).getName());
            TextView description = (TextView) findViewById(R.id.description);
            description.setText(Assets.getFoodDrinkList().get(listID).getDescription());
        }else if(listType.equals(CampusPlace.ListType.GENERAL)){
            TextView placeName = (TextView) findViewById(R.id.placeName);
            placeName.setText(Assets.getGeneralLocationList().get(listID).getName());
            TextView description = (TextView) findViewById(R.id.description);
            description.setText(Assets.getGeneralLocationList().get(listID).getDescription());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_selected_campus_place, menu);
        return true;
    }

    /** Called when the user clicks the map to location button */
    public void selectMapToLocation(View view) {
        //Ensures the user has granted location permissions before mapping
        if(Permissions.isLocationPermissionsGranted(this)) {
            Intent intent = new Intent(this, MapToLocationActivity.class);
            intent.putExtra("TYPE", listType);
            String message = "" + listID;
            intent.putExtra("ID", message);
            startActivity(intent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
